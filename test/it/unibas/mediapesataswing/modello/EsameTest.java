package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;

import org.junit.Test;

public class EsameTest {

	@Test (expected = IllegalArgumentException.class)
	public void testSetVotoHigherThan30ShouldThrowException() {
		Esame itss = new Esame("ITSS", 31, false, 9);
		itss.setVoto(31);
	}
	@Test (expected = IllegalArgumentException.class)
	public void testSetVotoLowerThan18ShouldThrowException() {
		Esame itss = new Esame("ITSS", 16, false, 9);
		itss.setVoto(16);
	}
	
	@Test 
	public void testSetVoto() {
		Esame itss = new Esame("ITSS", 20, false, 9);
		itss.setVoto(20);
		assertEquals(20,itss.getVoto());
	}
	@Test
	public void testSetCrediti() {
		Esame itss = new Esame("ITSS", 20, false, 9);
		itss.setCrediti(6);
		assertEquals(6,itss.getCrediti());
	}
	@Test (expected = IllegalArgumentException.class)
	public void testSetCreditiShouldThrowException() {
		Esame itss = new Esame("ITSS", 20, false, 9);
		itss.setCrediti(-1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetLodeShouldThrowException() {
		Esame itss = new Esame("ITSS", 20, false, 9);
		itss.setLode(true);
	}
	
	@Test
	public void testSetLode() {
		Esame itss = new Esame("ITSS",30, false, 9);
		itss.setLode(true);
		assertTrue(itss.isLode());
	}
	
	@Test
	public void testObjectToString() {
		Esame itss = new Esame("ITSS", 30, true, 9);
		assertEquals("Esame di ITSS (9 CFU) - voto: 30 e lode", itss.toString());
	}

	@Test
	public void testObjectToStringWithoutLode() {
		Esame itss = new Esame("ITSS", 30, false, 9);
		assertEquals("Esame di ITSS (9 CFU) - voto: 30", itss.toString());
	}
}
