package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;

import org.junit.Test;

public class StudenteTest {

	@Test (expected = IllegalArgumentException.class)
	public void getMediaPesataShouldThrowException() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		studente.getMediaPesata();
	}
	
	@Test 
	public void getMediaPesataShouldReturn30() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		studente.addEsame("ITSS", 30, false, 9);
		assertEquals(30.0, studente.getMediaPesata(), 0.001);
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void eliminaEsame3ShouldThrowException() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		studente.eliminaEsame(3);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void eliminaEsameNegativeIndexShouldThrowException() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		studente.eliminaEsame(-1);
	}
	
	@Test
	public void testEliminaEsame() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		studente.addEsame("ITSS", 30, false, 9);
		studente.eliminaEsame(0);
		assertEquals(0, studente.getNumeroEsami());
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetEsameShouldThrowException() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		studente.getEsame(0);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetEsameNegativeIndexShouldThrowException() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		studente.getEsame(-1);
	}
	
	@Test
	public void testGetEsame() throws Exception {
		Studente studente = new Studente("Elio", "Sisto", 123);
		Esame itss = new Esame("ITSS", 30, false, 9);
		studente.addEsame(itss);
		assertEquals(itss, studente.getEsame(0));
	}
}
