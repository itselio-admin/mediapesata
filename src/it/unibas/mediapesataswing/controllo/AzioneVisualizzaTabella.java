package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;
import javax.swing.AbstractAction;

public class AzioneVisualizzaTabella extends AbstractAction {

    private Controllo controllo;
    
    public AzioneVisualizzaTabella(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Tabella Esami");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Visualizza gli esami sotto forma di tabella");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_L));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Vista vista = this.controllo.getVista();
        PannelloPrincipale pannelloPrincipale = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE);
        this.setEnabled(false);        
        this.controllo.getAzione(Costanti.AZIONE_VISUALIZZA_LISTA).setEnabled(true);        
        pannelloPrincipale.passaATabella();
    }
}
